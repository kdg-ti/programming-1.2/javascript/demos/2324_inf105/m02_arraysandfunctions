//"use strict";
let friends = ["john", "pete", "george", "cornelius"];
console.log("entire array: " + friends);

console.log("Classical for loop");
for (let i = 0; i < friends.length; i++) {
  console.log(`Entry ${i}: ${friends[i]}`);
}
console.log("enhanced for of loop");

let index = 0;
for (const friend of friends) {
  console.log(`Entry ${index++}: ${friend}`);
}

const last = friends.pop();
console.log(`lost friend ${last}. Our members are ${friends}`);
friends.push("Bart");
console.log(`The gang of four: ${friends}`);

friends[1000]="ringo";
console.log(`array with a member at ${friends.length-1}: ${friends}` );
console.log("empty spot: "+ friends[910])



let explanation = "It returns a new array copying to it all items from index start to end (not including end). Both start and end can be negative, in that case position from array end is assumed."
let words = explanation.split(" ");
console.log(words);
console.log(`Words 5 to 10: ${words.slice(5, 10)}`);
console.log(`Words 5 to all but last 5: ${words.slice(5, -5)}`);

let wordsSplice = explanation.split(" ");
console.log(`SPLICE RESULT ${wordsSplice.splice(5, 10)} 
  AND REMAINING ARRAY ${wordsSplice}`);
let wordsSpliced = explanation.split(" ");
console.log(`TOSPLICED RESULT ${wordsSpliced.toSpliced(5, 10)} 
  AND REMAINING ARRAY ${wordsSpliced}`);

let numbers = [3, 4, 5, 6, 7, 8, 9, 10];
let sum = 0;
numbers.forEach(element => sum += element);
console.log("Sum with foreach: " + sum);

console.log("Words with an e", words.filter(el => el.includes("e")));
console.log("lowercase words", words.map(el => el.toLowerCase()));
console.log("length of words", words.map(el => `${el} length = ${el.length}`));
console.log("Words sorted",
  words.map(el => el.toUpperCase())
    .filter(el => el.includes("E"))
    // using if to compare
    // .sort((a, b) => {if(a>b){return 1;}else{return -1;}})
    //using a ternary expression to compare
    .sort((a, b) => (a > b) ? 1 : -1)
  //  .sort((a, b) => a - b)
);

// without fluid programming style
let upperCasewords = words.map(el => el.toUpperCase());
let filtered = upperCasewords.filter(el => el.includes("E"));
let final = filtered.sort((a, b) => (a > b) ? 1 : -1);
console.log("without fluid programming style:" + final);


console.log("sum using reduce",
  numbers.reduce((sum, element) => sum += element));
numbers=[1,2,3];
console.log("sum product using the index in the array (this should be 0*1+1*2+2*3=8" +
  numbers.reduce((sum, element, index) => sum += element * index)
);
// reduce takes the first element as the starting value
console.log("concatenating words: ", words.reduce((sum, element) => sum += "  " + element.toUpperCase()));
// specifying the  starting value
console.log( words.reduce(
  (sum, element) => sum += "  " + element.toUpperCase(),
  "concatenating words with a starting value: ")
);

console.log("Correct sum product using the index in the array (this should be 0*1+1*2+2*3=8), result: " +
  numbers.reduce((sum, element, index) => sum += element * index,
    0)
);

