function sum(numbers) {
  let sum = 0;
  numbers.forEach(element => sum += element);
  return sum;
}

console.log("Sum with array" + sum([1, 2, 3, 4, 5]));

function sumRest(...numbers) {
  let sum = 0;
  numbers.forEach(element => sum += element);
  return sum;
}

console.log("Sum with rest parameters" + sumRest(1, 2, 3, 4, 5));

const first5 = [1,2,3,4,5];
const next5=[6,7,8,9,10];
const first10=[first5,next5];

console.log("first and next 5 ",first10);
const first10spread = [...first5,...next5];
console.log("first and next 5 spread",first10spread);
console.log("first and next 5 spread",...first10spread);

let original = [11,12,13,14,15];
let duplicate = original;
duplicate.push(16);
console.log("the original is changed " + original);
original = [11,12,13,14,15];
let realDuplicate = [...original];
realDuplicate.push(16);
console.log("the original is  " + original);




