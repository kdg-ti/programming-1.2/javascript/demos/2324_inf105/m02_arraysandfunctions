function powerIterate(base,exponent) {
  let result=1;
  for (let i = 0; i < exponent; i++) {
    result*=base;
  }
  return result;
}

function power(base,exponent) {
  if (exponent===1) {
    return base;
  } else{
    return power(base,exponent-1)*base;
  }
}

console.log(powerIterate(5, 4));
console.log(power(5, 4));

function isMultipleof(number,divisor){
  if(number < 0){
    number = - number
  }
  if (number > divisor){
    return isMultipleof(number-divisor,divisor);
  } else{
    return number === divisor || number === 0;
  }
}
console.log("25 is a multiple of 5: " +isMultipleof(25,5));
console.log("24 is a multiple of 5: " +isMultipleof(24,5));
console.log("-25 is a multiple of 5: " +isMultipleof(-25,5));
console.log("24 is a multiple of 5: " +isMultipleof(-24,5));

