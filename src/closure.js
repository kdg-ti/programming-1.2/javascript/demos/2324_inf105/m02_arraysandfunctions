function makeCounter(){
  let count = 0;
  return  function (){
    return count++
  }
}
const counter = makeCounter();
const anotherCounter = makeCounter();
console.log("counter ", counter());
console.log("counter again", counter());
// we do not have access to the count variable here!
//console.log("the count variable is ",count);
console.log("another counter ", anotherCounter());
console.log("counter again and again", counter());
